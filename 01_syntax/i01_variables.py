def variables():
    variable_one = 10
    variable_two = 20
    result = variable_one + variable_two
    print(result)
    print('...')

    number = 11
    print(number)
    number = "changed to string type"
    print(type(number))  # type() function will show the type of variable/obj etc.
    print('...')

    my_income = 2000
    my_tax_rate = 0.3
    earning = my_income * my_tax_rate
    print(earning)
    print(type(earning))
    print('...')

    a, b, c = 100, 100_000, 10_000_000  # we can separate large numbers for better visibility with current syntax
    print(a, b, c)


def typed_variables():
    # you can specify the type of the variables
    name: str = 'Ihor'
    age: int = 35
    weight: float = 85.3
    alcoholic: bool = False
    print(name, age, weight, alcoholic)


if __name__ == '__main__':
    # variables()
    typed_variables()
