def basis():
    hello = 'hello'
    world = " world"
    print(hello + world)
    print(len(hello))  # len() function shows amount of characters in the string
    print(len(world))

    print("how are you?\nI'm fine")  # \n operator move upcoming text to the new line
    print("are you sure?\n\tOf course! Please don't bothering me")  # \t operator adds TAB before upcoming text(four spaces)


def indexing():
    full_name = 'Choliy Ihor Volodymyrovych'
    print(full_name[5])  # get character by index 5
    print(full_name[7:])  # get all characters from index 7
    print(full_name[:6])  # get all characters till index 6(6 is not included)
    print(full_name[7:11])  # get all characters from index 7 till index 11(11 is not included)
    print(full_name[::2])  # get all characters with step of 2(whitespace is a character as well)
    print(full_name[3:15:2])  # get all characters from 3 to 15 indexes with step of 2
    print(full_name[-1])  # get last character
    print(full_name[-3])  # get third character from back
    print(full_name[::-1])  # reverse string


def formatting():
    name = 'Sam'
    name = 'P' + name[1:]  # changing first letter
    print(name)
    print('...')

    text_one = 'Hello World!'
    text_two = " It's so beautiful outside!"
    print(text_one + text_two)  # concatenation of two strings
    print('...')

    # concatenation/dividing with numbers is not allowed for strings('one' + 15) or ('one' / 1.2)
    # but we can use multiplying
    one = 'one' * 5  # will multiply text "one" five times, look at the console result
    print(one)
    print('...')

    # if we have to concatenate strings with other types we should do the following:
    name = 'Ihor'
    age = 34
    print(f"Hello! My name is {name} and I'm {age} years old")  # just adding f before string
    print('...')

    # old fashion way to do the same
    print('Name: {}, Age: {}'.format(name, age))
    print('Name: {1}, Age: {0}'.format(name, age))  # you can put index of args to change the order of strings
    print('Name: {a}, Age: {n}'.format(n=name, a=age))  # you can assign custom index for string arguments


def functions():
    hello = 'Hello My Dear Friend!'
    print(hello.upper())  # upper case
    print(hello.lower())  # lower case
    print(hello.split())  # split by spaces and convert to list of strings
    print(hello.split('e'))  # split by current character
    print(hello.partition('My'))  # split to three part, 'My' is a middle one
    print('...')

    full_name = 'ihor choliy'
    print(full_name.capitalize())  # capitalize first word
    print(full_name.title())  # capitalize every single word
    print('...')

    greeting = 'Hello! How are you?'
    print(greeting.count('o'))  # amount of 'o' in string
    print(greeting.find('o'))  # returns first index of 'o', -1 if no such character/word founded


def boolean():
    print('hello123'.isalnum())  # is string alphanumeric
    print('hey you'.isalnum())  # false because string contains whitespace
    print('112233'.isnumeric())  # is string numeric
    print('Ihor Choliy'.islower())  # is all characters in lowercase
    print('of course'.isspace())  # is all whitespaces
    print('why not'.endswith('t'))


def main():
    # basis()
    # indexing()
    # formatting()
    # functions()
    boolean()


if __name__ == '__main__': main()
