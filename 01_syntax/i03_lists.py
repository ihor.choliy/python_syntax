from random import shuffle


def indexing():
    num_list = [1, 1, 3, 5, 7]
    str_list = ['one', 'two', 'three']
    mix_list = [3, 'hello', 3, 104.33, False]  # you can add different type of variables/objects to list
    print(num_list)
    print(str_list)
    print(len(mix_list))  # amount of elements in the list
    print(mix_list[0])  # get the fist element from the list
    print(mix_list[-1])  # get the last element from the list
    print(mix_list[1:])  # get all elements start from index 1
    print(mix_list[:2])  # get all elements till index 2(2 is not included)


def editing():
    print('LIST CONCATENATION')
    list_one = ['one', 2, 'three']
    list_two = [5, 6, 'seven']
    list_sum = list_one + list_two  # you can concatenate two lists exactly like strings
    list_one.extend(list_two)  # or use extend() function
    print(list_sum)
    print(list_one)
    print('...')

    print('CHANGING VALUES IN THE LIST')
    list_sum[0] = list_sum[0].upper()  # all values in list are mutable, so you can change them dynamically
    list_sum[-1] = list_sum[-1].upper()
    list_sum[1] = 'two'  # you can change even the type of variables/objects in the list
    print(list_sum)
    print('...')

    print('ADDING VALUES')
    list_sum.append(8)  # adding new value at the end of the list
    list_sum.append('nine')
    list_sum.insert(2, 'two and half')  # insert element at index(not removing current)
    print(list_sum)
    print('...')

    print('REMOVING VALUES')
    list_sum.remove('nine')  # remove first occurrence of value
    item_1 = list_sum.pop(1)  # remove value by index 1, pop() function returns removed item
    item_last = list_sum.pop()  # remove last value(by default -1)
    print(item_1)
    print(item_last)
    print(list_sum)
    print('...')

    print('LIST MULTIPLICATION')
    new_list = list_sum * 3
    print(new_list)


def sorting():
    names = ['Ihor', 'Bobby', 'John', 'Andrew', 'Suzanne']
    numbers = [33.4, 12.1, 7, 42.87, 104.112]
    names.sort()  # sort() function didn't return sorted list, so you can't assign sorted list to new variable
    numbers = sorted(numbers)  # but this one, build in sorted() function, actually returns sorted list
    print(names)
    print(numbers)
    print('...')

    names.reverse()  # reverse() function didn't return reversed list as well
    numbers.reverse()
    print(names)
    print(numbers)
    print('...')

    print(names[::-1])  # return reversed list of names
    print(numbers[::-1])  # return reversed list of numbers


def zipping():
    numbers = [1, 2, 3, 4, 5, 6, 7]
    letters = ['a', 'b', 'c', 'd']
    values = [100, 200, 300, 400]

    # zip function combine lists by pairs, and put this pairs to tuples
    # if in some list were more items than is other, zip() function will ignore them
    # zip function returns link to the memory socket
    first_zip = zip(numbers, letters, values)
    print(first_zip)

    # converting zip to list of tuples:
    combined_list = list(first_zip)
    print(combined_list)


def functions():
    numbers = [1, 2, 3, 3, 3, 4, 5, 6, 7, 8, 9, 10]

    # min-max functions
    print(min(numbers))
    print(max(numbers))
    print('...')

    # insert element at index(not removing current)
    numbers.insert(1, 'two')
    print(numbers)
    print('...')

    # reverse item positions
    numbers.reverse()  # or numbers[::-1]
    print(numbers)
    print('...')

    # shuffle() function
    # it should be imported from 'random' library
    # randomizing value positions in the list
    shuffle(numbers)  # shuffle() function didn't return shuffled list
    print(numbers)
    print('...')

    # counting
    print(numbers.count(3))  # amount of elements in the list
    print(numbers.count(27))
    print(numbers.count(1))
    print('...')

    words = ['one', 'two', 'three']
    numbers.extend(words)  # the same as: numbers + words
    print(numbers)
    print(numbers.index('one'))  # return first index of the value
    print(numbers.clear())  # clear whole list


def main():
    # indexing()
    # editing()
    # sorting()
    # zipping()
    functions()


if __name__ == '__main__': main()
