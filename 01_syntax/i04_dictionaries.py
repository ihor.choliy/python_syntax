def basics():
    # keys & values of the dictionaries could be any type, but keys have to be unique
    # you can add to dictionary whatever you want - objects, lists, even dictionaries, example on line 12
    first_dict = {'apple': 5.99, 'orange': 7.99, 'milk': 'n/a', 'bread': 11.5}
    print(first_dict['apple'])
    print(first_dict['milk'])


def get_data():
    some_list = [15.5, 'hello', False, 321]
    dict_one = {11: 'Ihor', 22: 107.23, 33: True, 44: 17}  # you can use integer/float for keys in dictionary as well
    dict_new = {'a': False, 'b': 22.3, 'c': some_list, 'd': dict_one}
    print(dict_new)  # show whole dictionary
    print(dict_new['d'])  # show value by key in dictionary
    print(dict_new['d'][33])  # show value by nested key of the nested dictionary
    print('...')

    dict_new['c'][1] = dict_new['c'][1].upper()  # every value in dictionary is mutable as well as in the list
    dict_new['d'][11] = dict_new['d'][11].upper()  # string is immutable, so we have to assign new string
    print(dict_new)


def adding_or_changing():
    num_dict = {'n1': 100, 'n2': 200}
    print(num_dict)
    num_dict['n3'] = 300
    print(num_dict)
    num_dict['n2'] = "hello there"  # you can change the type of dictionary value dynamically, as well as in the list
    print(num_dict)


def functions():
    numbers = {'k1': 10, 'k2': 20, 'k3': 30, 'k4': 40}
    print(numbers.items())  # shows whole items
    print(numbers.keys())  # shows only keys
    print(numbers.values())  # shows only values


def merging():
    numbers = {'k1': 10, 'k2': 20, 'k3': 30, 'k4': 40}
    strings = {'s1': 'hello', 's2': 'my', 's3': 'name', 's4': 'is', 's5': 'ihor'}
    keys_duplication = {'k1': 0, 's1': 'go and fuck yourself'}
    print(strings)
    strings.update(numbers)  # update() function didn't return merged dictionary, so you couldn't assign it to new variable
    print(strings)
    strings.update(keys_duplication)  # if there are key(s) duplication, update() function will override values by this keys
    print(strings)


def main():
    # basics()
    # get_data()
    # adding_or_changing()
    # functions()
    merging()


if __name__ == '__main__': main()
