def basics():
    # tuples are very similar to lists with only one key difference - immutability
    # it means, values in tuples cannot be changed,
    # and instead of using square braces, tuples are using parenthesis
    some_list = ['one', 2, True, 4, 'five']
    some_tuple = ('one', 2, True, 4, 'five')
    print(type(some_list))
    print(type(some_tuple))
    print(some_tuple[2])  # getting value from a tuple, the same as in the list
    print('...')

    # you cannot change any value in tuple like below, you'll get TypeError exception
    # some_tuple[3] = "four"


def functions():
    new_tuple = 'a', 'a', 1, 'a', 1, 'b'  # you can create tuple without parenthesis
    print(new_tuple.count('a'))  # returns amount of 'a' in tuple
    print(new_tuple.count(1))  # returns amount of 1 in tuple
    print(new_tuple.index('a'))  # returns index of first 'a' position in tuple

    # clearing tuple
    print(new_tuple)  # current tuple
    new_tuple = ()  # there is no clear() function in tuple, that is why we have to assign new empty tuple
    print(new_tuple)


def main():
    # basics()
    functions()


if __name__ == '__main__': main()
