def basics():
    # set is completely the same as list except two key differences:
    # - it can contain only unique values/objects
    # - values in set are unordered, so you can't access values by index
    set_one = set()  # this is how to create a set
    set_two = {'a', 'a', 'b'}  # or like this one, second string 'a' will not be added
    set_one.add(3)
    set_one.add(4)
    set_one.add(3)  # second integer 3 will not be added
    print(type(set_one))
    print(type(set_two))
    print(set_one)
    print(set_two)
    print('...')

    # adding list to set
    some_list = [1, 1, 2, 3, 3, 'one', 'one', 'yes', 'no', 'yes']
    set_one.update(some_list)  # it will add only unique values
    print(set_one)
    print('...')

    # changing values in the set
    # as we already know, values in set are unordered, so we cannot update values by there index
    # if we have to do current operations we can do the following:
    set_one.remove(2)
    set_one.add('TWO')
    print(set_one)
    print('...')


def functions():
    set1 = {0, 1, 2, 3, 4, 5}
    set2 = {0, 1, 2, 3, 4, 5, 6, 7}
    set3 = set2.difference(set1)  # return the difference of sets as a new set
    print(set3)

    set4 = set1.union(set2)  # return the union of sets as a new set
    print(set4)

    set6 = {1, 3, 'four', 7, 'ten'}
    set7 = {5, 0, 9, 1, 'four'}
    set6.difference_update(set7)  # remove all identical elements that another set contains from current set
    print(set6)

    set6.discard('six')  # remove 'six' if exists, if not - do nothing
    print(set6)

    set7 = set1.intersection(set2)  # create a new set that contains same elements from both sets
    print(set7)
    print(set6.isdisjoint(set7))  # return true if two sets don't have same elements


def main():
    # basics()
    functions()


if __name__ == '__main__': main()
