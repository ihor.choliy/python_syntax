file_path = 'C:\\Users\\ihorc\\Desktop\\'  # in python for Windows OS path we have to add double slashes
file_name = 'first_text_file'
file_extension = '.txt'
# we can write all path in one string line, this is mostly for future flexibility editing
full_file_path: str = f'{file_path}{file_name}{file_extension}'


def basics():
    # open() function will create new file by current path, with current name & file extension
    # w+ operator creates new file if it doesn't exist, or update if it's already created
    text_file = open(full_file_path, "w+")
    text_file.write('first line\nsecond line\nthird line')  # write text to the text file
    text_file.seek(0)  # reset cursor to start position
    print(text_file.read())
    print(text_file.read())  # this text file will not be shown(only empty space), explanation below:
    # we have to reset cursor (seek(0)) every time after file reading,
    # if we want to read this file again, and we didn't reset the cursor,
    # it will be at the end of the text, and nothing will be shown

    text_file.seek(0)  # reset cursor to start position
    lines_list = text_file.readlines()  # this function returns the list of string lines
    print(lines_list)

    # after finishing working with current file we have to close() it,
    # otherwise we wouldn't have possibility to remove or edit it from other application
    text_file.close()
    print('...')


# in this function will be shown another approach, without needs to close() file stream
def extra():
    with open(full_file_path, 'r') as name_one:  # this construction will provide for us file stream auto closing
        file_content = name_one.read()
        print(file_content)
        print('...')

    # file mode types explanation:
    # r  - read only
    # w  - write only(create new file)
    # a  - append only
    # r+ - read and write
    # w+ - read and write(override existing files or create a new one)

    # append new line to file
    with open(full_file_path, 'a') as name_two:  # we could name this variable whatever we want
        name_two.write('\nfourth line')

    # read updated file
    with open(full_file_path, 'r') as name_three:
        name_three.seek(0)
        print(name_three.read())


def main():
    basics()
    # extra()


if __name__ == '__main__': main()
