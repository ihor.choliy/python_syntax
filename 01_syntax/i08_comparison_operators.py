def comparison_operators():
    print(f"2 equals 3? {2 == 3}")
    print(f"5.0 equals 5? {5.0 == 5}")  # python doesn't care about types, it checks values
    print(f"'one' equals 'one'? {'one' == 'one'}")
    print(f"'two' not equals 2? {'two' != 2}")
    print(f"7 more than 9? {7 > 9}")
    print(f"3 less than 5? {3 < 5}")
    print(f"100 more or equals 99? {100 >= 99}")
    print(f"333 less or equals 111? {333 <= 111}")


if __name__ == '__main__': comparison_operators()
