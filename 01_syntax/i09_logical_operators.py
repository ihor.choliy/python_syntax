def logical_operators():
    print(1 < 2 < 3)  # the chain of comparison operators we can write like this(bad practice)
    print(1 < 2 and 2 < 3)  # or to make it more readable we can use logical operator instead
    print('a' == "a" and 9.0 <= 7)  # all conditions have to be true
    print(77 >= 60 or 'a' == 'b')  # at least one of the conditions have to be true
    print(3 != 3)  # same result but different syntax as in example below
    print(not 3 == 3)  # we can use logical operator if needed(in some cases it could be more readable)


if __name__ == '__main__': logical_operators()
