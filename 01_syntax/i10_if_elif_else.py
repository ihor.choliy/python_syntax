def if_elif_else():
    # super simple if/else condition
    condition = True
    if condition:
        x = 1
    else:
        x = 0
    print(condition, x)

    # example of one line if/else condition
    x = 10 if condition else 0
    print(condition, x)

    # if/elif/else construction
    name = 'Ihor'
    if name == 'Bobby':  # you can use comparison & logical operators in if/elif statements
        print('Hello Bobby!')
    elif name == 'Andrew':
        print('Hello Andrew!')
    elif name == 'Mike' or name == 'Ihor':
        print('Hello Ihor!')
    else:
        print('Who are you?')


if __name__ == '__main__': if_elif_else()
