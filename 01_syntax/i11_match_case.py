def match_case():
    difficulty: int = 5
    text: str
    match difficulty:
        case 1:
            text = "Easy"
        case 2:
            text = "Medium"
        case 3:
            text = "Hard"
        case _:  # if nothing above matches
            text = "No such difficulty"
    print(text)


if __name__ == '__main__': match_case()
