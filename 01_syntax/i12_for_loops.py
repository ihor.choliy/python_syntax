def for_loops():
    # super simple for loop
    my_list = [7, 'hello', 45.91, True]
    for item in my_list:  # we can use any name placeholder instead of 'item'
        print(item)

    # for getting index of every item in the list we should use enumerate() function in for loop
    for index, value in enumerate(my_list):  # here we can use any names for 'index' & 'value' as well
        print(f'index of value {value} is: {index}')

    # for loop with tuples
    list_of_tuples = [('one', 2, 3.1), ('four', 5.2, 6), (7, 'eight', 9.3)]
    for item in list_of_tuples: print(item)  # whole tuple will be shown

    # this is tuple unpacking, if we want to get tuple items
    for a, b, c in list_of_tuples:  # if in one of tuples will be less than three items - error will be thrown
        print(f'first: {a}, second: {b}, third: {c}')

    # for loop with dictionaries
    my_dictionary = {'k1': 1, 'k2': 2, 'k3': 3}
    for item in my_dictionary: print(item)  # only keys will be shown
    for item in my_dictionary.items(): print(item)  # key/value pairs will be shown
    for item in my_dictionary.values(): print(item)  # only values will be shown
    for key, value in my_dictionary.items():  # key & value data access
        print(f'key: {key}, value: {value}')

    # for loop through few lists
    names = ['Peter Parker', 'Clark Kent', 'Wade Wilson', 'Bruce Wayne']
    heroes = ['Spiderman', 'Superman', 'Deadpool', 'Batman']
    universes = ['Marvel', 'DC', 'Marvel', 'DC']

    for item in zip(names, heroes, universes):  # unpacking by tuples
        print(item)

    for name, hero, universe in zip(names, heroes, universes):  # unpacking by tuples values
        print(f'{name} is actually {hero} from {universe} universe')


if __name__ == '__main__': for_loops()
