def while_loop():
    x = 0
    while x < 5:
        print(f'the value of X is: {x}')
        x += 1
    else:
        print('loop is ended')  # else condition will always run, we can remove it if we don't need it


def loop_statements():
    # pass - does nothing at all, just moving to the next interation or code below(mostly for debugging purpose)
    # continue - skip current loop iteration
    # break - stops current loop and get out of it

    # pass statement example
    x = [1, 2, 3]
    for _ in x:  # if we are not using loop item we should name it '_'
        # for example, you will add some logic later,
        # and for now just want to compile current script, you could add 'pass' statement
        print(x)
        pass

    # continue statement example
    name = 'Ihor'
    for letter in name:
        # for example, we don't want to print 'h' letter, so we can skip current iteration with 'continue' statement
        if letter == 'h': continue
        print(letter)

    # break statement example:
    number = 0
    while 0 < 777:
        if number == 5: break  # break statement will close current loop at all
        print(number)
        number += 1


def main():
    # while_loop()
    loop_statements()


if __name__ == '__main__': main()
