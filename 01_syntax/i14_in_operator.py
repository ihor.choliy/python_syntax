def in_operator():
    print('x' in ['x', 'y', 'z'])
    print('o' not in 'Ihor')
    print('...')

    dictionary = {'key1': 111, 'key2': 222}
    print('key1' in dictionary.keys())  # is 'key1' in dictionary keys
    print(333 in dictionary.values())  # is 333 in dictionary values
    print('...')

    # range function
    print(3 in range(1, 10))  # is 3 in range from 1 to 10
    print('...')

    # 1 - index from
    # 2 - index to(not included)
    # 3 - with step
    for number in range(2, 10, 2): print(number)


if __name__ == '__main__': in_operator()
