def list_comprehensions():
    hello = 'hello'
    my_list = []
    # for example, we have to create a list from current string
    # typical syntax for this task:
    for letter in hello: my_list.append(letter)
    print(my_list)

    # the same task but with list comprehensions:
    my_list = [letter for letter in hello]  # first 'letter' adds item to the list(the same as: 'my_list.append(letter)')
    print(my_list)

    # another example
    my_list = [x for x in 'Ukraine']
    print(my_list)

    # another example:
    # we could add logic to the first 'num' and 'if statement' at the end of the list comprehension
    my_list = [num * 2 for num in range(0, 12) if num % 2 == 0]
    print(my_list)

    # if we want to add 'else' block as well, the syntax and blocks position will be slightly different
    my_list = [num * 2 if num % 2 == 0 else 'other' for num in range(0, 12)]
    print(my_list)

    # more complex calculation in first block
    celsius = [15, 19, 22, 29]
    fahrenheit = [((9 / 5) * temp + 32) for temp in celsius]
    print(fahrenheit)

    # the same as above but without list comprehension syntax
    # this two approaches have completely the same performance
    fahrenheit = []
    for temp in celsius: fahrenheit.append((9 / 5) * temp + 32)
    print(fahrenheit)

    # nested loops typical syntax:
    my_list = []
    for x in [2, 4, 8]:
        for y in [10, 100, 1000]:
            my_list.append(x * y)
    print(my_list)

    # the same as above but with list comprehension approach:
    my_list = [x * y for x in [2, 4, 8] for y in [10, 100, 1000]]
    print(my_list)


if __name__ == '__main__': list_comprehensions()
