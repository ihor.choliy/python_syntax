# super simple function
def show_user_info(name, age):
    print(f"Hello! My name is {name} and I'm {age} years old")


# you can specify default values for function arguments
def show_car_info(brand='BMW', model='i3'):
    print(f'Car brand: {brand}, model: {model}')


# you can specify the type of function arguments(not necessary)
def celsius_to_fahrenheit(celsius: int):
    fahrenheit = (9 / 5) * celsius + 32
    return int(fahrenheit)  # cast float to int & returns it


# return amount of even numbers in the list
def check_even_amount(num_list: []):
    amount = 0
    for num in num_list:
        if num % 2 == 0:
            amount += 1
        else:
            pass
    return amount


# check who is employee of the month
def get_top_employee(employees: []):
    employee_name = ''
    working_hours = 0
    for name, hours in employees:
        if hours > working_hours:
            employee_name = name
            working_hours = hours
        else:
            pass
    return employee_name, working_hours  # returns tuple


# unpacking tuple into variables
def show_top_employee(employees):
    name, hours = get_top_employee(employees)
    print(f'The employee of the month is {name}\nHours amount: {hours}')


# you can specify return type of the function(not necessary)
def action_01(var_01: float, var_02: float) -> [str]:
    return list(str(var_01 * var_02))


# another example
def action_02(name: str, age: int) -> str:
    return f"Hello! My name is {name} and I'm {age} years old"


# use keyword 'None' if you don't return anything(not necessary)
def action_03(numbers: list) -> None:
    print([num for num in numbers if num % 2 == 0])


# nested function(usage in func_02() and main())
def func_01() -> str: return 'Hello!'


# syntax for using nested function
def func_02(name: str, function):
    print(function())
    print(f'My name is {name}')


def main():
    show_user_info('Ihor', 34)
    print('...')

    show_car_info('Audi', 'a4')
    show_car_info(model='x7')
    show_car_info()
    print('...')

    fahrenheit = celsius_to_fahrenheit(15)
    print(fahrenheit)
    print('...')

    amount = check_even_amount([1, 4, 7, 8, 10, 11, 15, 20])
    print(amount)
    print('...')

    employees = [('Bob', 105), ('Emmy', 98), ('Mike', 165), ('Jessie', 145), ('Linda', 120)]
    show_top_employee(employees)
    print('...')

    print(action_01(15.5, 11.3))
    print(action_02('Ihor', 34))
    action_03([1, 2, 3, 4, 5, 6, 7, 8, 9])
    print('...')

    # example of usage nested unction
    func_02('Ihor', func_01)


if __name__ == '__main__': main()
