# we can enter any placeholder instead of 'args',
# but python code conventions recommends using this name
def salary_tax(*args):
    tax = sum(args) * 0.25
    print(f'Summary tax for all employees is: {tax}')


def employees_names(*args):
    for employee in args: print(employee)


# you can specify the type of arguments(not necessary)
def cars_in_storage(*cars: str) -> None:
    [print(car) for car in cars]


# the same as in example above we can use any placeholder we want,
# but python code conventions recommends using 'kwargs' name for key-value arguments
# and the same as in *args you can specify the type on *kwargs(not necessary)
def fruit_magazine(**kwargs: float) -> None:
    for name, price in kwargs.items():  # tuples unpacking in action
        print(f'Price of {name} is: {price}')


# we can use *args and **kwargs in combination
def show_some_data(*args, **kwargs):
    for args_item in args: print(args_item)
    print('...')
    for kwargs_item in kwargs.items(): print(kwargs_item)


def main():
    salary_tax(1500, 1900, 1300, 2100, 2500)  # we can pass any amount of arguments
    print('...')

    employees_names('John', 'Sandy', 'Mike', 'Jeff')
    print('...')

    cars_in_storage('Audi', 'BMW', 'Ford', 'Honda', 'Toyota')
    print('...')

    fruit_magazine(apple=5.99, orange=3.99, banana=4.99)
    print('...')

    show_some_data(10, 'yes', 55.6, 'ok', True, Ihor='nice', Gregory=111, Suzanne='awesome')
    print('...')


if __name__ == '__main__': main()
