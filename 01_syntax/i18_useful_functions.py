from random import randint


def randint_func():
    # randint() function should be imported from python 'random' library
    random_int = randint(0, 1000)  # generates random integer from 0 to 1000
    print(random_int)


def casting_func():
    string_value = '5'
    print(type(string_value))

    # casting to float
    float_value = float(string_value)
    print(type(float_value))

    # casting to int
    int_value = int(string_value)
    print(type(int_value))


def input_func():
    # this function returns string that you have entered in console
    name = input('Please enter your name: ')
    age = input('Please enter your age: ')
    print(f'Name: {name}')
    print(f'Age: {age}')


def min_max():
    print(min(5, 11, 7, 10))  # returns the smallest item
    print(max(3, 2, 15))  # returns the largest item
    print(max([12, 0, 11.3, 7, 21.5]))  # returns the largest list item


# capitalize() and join() functions
def capitalize_words():
    text = 'hello my friend, how are you?'
    text: [str] = text.split()
    for index, word in enumerate(text):
        text[index] = word.capitalize()
    print(' '.join(text))

    # we can do the same much more simpler with title() function:
    print('hello my friend, how are you?'.title())


def main():
    # randint_func()
    # casting_func()
    # input_func()
    # min_max()
    capitalize_words()


if __name__ == '__main__': main()
