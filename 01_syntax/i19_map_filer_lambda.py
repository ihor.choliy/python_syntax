def square_num(num): return num ** 2


def capitalize_word(word: str): return word.capitalize()


def is_not_even(num: int): return not num % 2 == 0


def map_func():
    # what does map() function do:
    # applying function(first parameter) to every element in the list, set, dict etc.(second parameter)
    # very important note, we should pass function to map() without parenthesis, only function name
    mapped_numbers = map(square_num, [1, 2, 3, 4, 5, 6, 7])  # we can pass few lists, instead of one to map as well
    print(mapped_numbers)  # map() doesn't return list of mapped objects, only memory location link
    mapped_list = list(mapped_numbers)  # if we want to convert map() to list we should explicitly cast it to list
    print(mapped_list)
    print('...')

    # this is how to loop through map() or filter():
    for item in map(capitalize_word, ('ihor', 'mike', 'erik', 'liza')): print(item)
    print('...')


def filter_func():
    # what does filter() function do:
    # removing item from list, set, dict etc.(second parameter) if statement in the first parameter is False
    # the same as in the map(), we should pass only function name, without parenthesis
    filtered_numbers = filter(is_not_even, [1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
    print(list(filtered_numbers))
    print('...')


def lambda_example():
    # lambda function is an anonymous function that has no name
    # syntax of lambda have to be short, for being more readable
    # this lambda syntax -> 'lambda num: num ** 2' is completely equal to 'square_num(num)' function above
    mapped_numbers = map(lambda num: num ** 2, [1, 2, 3, 4, 5, 6, 7])
    print(list(mapped_numbers))

    # this lambda syntax -> 'lambda i: not i % 2 == 0' is completely equal to 'is_not_even(num)' function above
    filtered_numbers = filter(lambda i: not i % 2 == 0, [1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
    print(list(filtered_numbers))

    # one more lambda example with map() function that returns reversed words
    text = 'Hello! How are you?'
    print(list(map(lambda word: word[::-1], text.split())))


def main():
    # map_func()
    # filter_func()
    lambda_example()


if __name__ == '__main__': main()
