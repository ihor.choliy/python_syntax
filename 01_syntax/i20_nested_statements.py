# in python there is LEGB rule,
# python variables searching order:
# 1) L - LOCAL variables(in nested functions)
# 2) E - ENCLOSING variables(in functions)
# 3) G - GLOBAL variables(outside of the functions)
# 4) B - BUILD-IN variables(int, str, list, open etc.)
name = 'GLOBAL variable'


def print_variable():
    name = 'ENCLOSING variable'
    print_text(name)

    # in python there is an option to create nested functions
    def nested_function():
        name = 'LOCAl variable'
        print_text(name)

    nested_function()


def change_global_variable():
    # if we want to use/edit global variable from function,
    # even though this is a bad practice and much better to use enclosing/local variables with 'return' type,
    # we should type 'global' keyword before global variable name, and only then we could use/edit it
    global name
    print_text(name)
    name = 'CHANGED GLOBAL variable'
    print_text(name)


def print_text(text): print(f'Printed {text}')


def main():
    print_variable()
    change_global_variable()


if __name__ == '__main__': main()
