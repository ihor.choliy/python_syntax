class Car:
    # class attributes
    # same for any instance of the class
    airbags: bool = True
    steering_wheel: str = 'Left'

    # init block(constructor) calls every time when new instance of the class created
    def __init__(self, brand: str, model: str, year: int, accident: bool = False):
        self.brand = brand
        self.model = model
        self.year = year
        self.accident = accident

    # custom string representation of an object(similar to toString() function from Java/Kotlin)
    # always have to return a string(explicitly or not)
    def __str__(self) -> str:
        return f'Object name: {Car.__name__} | Memory hash: {Car.get_hash(self)}'

    # objects equality(equals)
    def __eq__(self, other):
        if other.__class__ is self.__class__:
            return (self.brand == other.brand and
                    self.model == other.model and
                    self.year == other.year and
                    self.accident == other.accident)
        else:
            return False

    # unique hash for every instance of an object
    def __hash__(self):
        return hash((self.__class__, self.brand, self.model, self.year, self.accident))

    # if you need to provide some custom length of an object
    # always have to return an int(explicitly or not)
    def __len__(self) -> int:
        return len(self.brand) + len(self.model) + self.year + int(self.accident)

    # if you need to do some extra stuff after instance of an object was deleted
    def __del__(self):
        print(f'{Car.__name__} object was deleted')

    # for class methods we always have to pass 'self' as a first parameter(exactly like in 'init' block)
    def show_info(self, extra_info: str = ''):
        print(f"Brand: {self.brand}\n"
              f"Model: {self.model}\n"
              f"Year: {self.year}\n"
              f"Accident: {self.accident}\n"
              f"Airbags: {self.airbags}\n"  # not common syntax of calling class attributes
              f"Steering wheel: {Car.steering_wheel}\n"  # more common syntax for calling class attributes
              f"Extra info: {'No extra info' if extra_info == '' else extra_info}\n"
              f"...")

    # simple function which returns memory hash
    def get_hash(self):
        full_path = str(hash(self)).split()
        return full_path[-1]


def main():
    audi = Car('Audi', 'A4', 2008)
    audi.show_info('Great family car')
    # we can pass parameters without order
    bmw = Car(year=2022, model='e46', brand='BMW', accident=True)
    bmw.show_info()

    print(bmw)  # calling __str__() function
    print(bmw.__len__())
    print(bmw.__hash__())
    print(audi.__eq__(bmw))
    del bmw  # calling __del__() function


if __name__ == '__main__': main()
