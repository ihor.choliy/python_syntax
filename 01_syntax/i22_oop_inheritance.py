from enum import Enum, auto


# this is how to create Enum in python
class Weapon(Enum):
    AXE = auto()  # auto() function will create id based on position in Enum
    SWORD = auto()
    MACE = auto()
    STAFF = auto()
    BOW = auto()
    CROSSBOW = auto()


class Race(Enum):
    HUMAN = auto()
    ELF = auto()
    DWARF = auto()
    ORC = auto()
    TROll = auto()
    HOBBIT = auto()


# parent class
class Character:

    def __init__(self, race: Race, name: str, age: int, weapon: Weapon):
        self.race = race
        self.name = name
        self.age = age
        self.weapon = weapon

    def show_character_info(self):
        print(f'Race: {self.race}\n'
              f'Name: {self.name}\n'
              f'Age: {self.age}\n'
              f'Weapon: {self.weapon}\n')

    def show_weapon_id(self):
        print(f'Weapon ID: {self.weapon.value}')  # this is how to get Enum id


# child class
class Elf(Character):

    def __init__(self):
        super().__init__(Race.ELF, 'Legolas', 1317, Weapon.BOW)
        # another example of parent constructor initialization located in Human class below

    def bow_shoot(self):
        print(f'{self.name} is shooting from the {self.weapon}')
        super().show_weapon_id()


# child class
class Human(Character):

    def __init__(self):
        # another example of parent constructor initialization
        # but in this case, we have to add 'self' as a first parameter parent in constructor
        Character.__init__(self, Race.HUMAN, 'Aragorn', 87, Weapon.SWORD)

    def sword_hit(self):
        print(f"{self.name} is hitting by {self.weapon}")
        super().show_weapon_id()


# child class
class Orc(Character):

    def __init__(self):
        super().__init__(Race.ORC, 'Azog', 247, Weapon.AXE)

    def axe_smash(self):
        print(f'{self.name} is smashing by {self.weapon}')
        super().show_weapon_id()


def main():
    elf = Elf()
    elf.show_character_info()
    elf.bow_shoot()
    print('...')

    human = Human()
    human.show_character_info()
    human.sword_hit()
    print('...')

    orc = Orc()
    orc.show_character_info()
    orc.axe_smash()
    print('...')


if __name__ == '__main__': main()
