class Animal:

    def __init__(self, name: str, age: int):
        self.name = name
        self.age = age

    # typical syntax for abstract method
    def voice(self): pass


class Dog(Animal):

    def voice(self):
        print(f'{self.name} says woof!')


class Cat(Animal):

    def voice(self):
        print(f'{self.name} says meow!')


class Cow(Animal):

    # we can initialize cow data in constructor
    def __init__(self):
        super().__init__('Cow', 9)

    def voice(self):
        print(f'{self.name} says moo!')


def main():
    dog = Dog('Dog', 5)
    cat = Cat('Cat', 7)
    cow = Cow()

    dog.voice()
    cat.voice()
    cow.voice()


if __name__ == '__main__': main()
