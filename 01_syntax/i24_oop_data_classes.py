import inspect
from dataclasses import dataclass
from pprint import pprint


# the same class as in 'i21_oop_basics.py' but with much less of code
#  if 'unsafe_hash=True' __hash__() method will be added
@dataclass(unsafe_hash=True)
class Car:
    brand: str
    model: str
    year: int
    accident: bool = False


def main():
    car = Car('Audi', 'A4', 2008)
    print(car)
    print(car.__hash__())
    print('...')

    # pprint() function shows all methods in current class
    pprint(inspect.getmembers(Car, inspect.isfunction))


if __name__ == '__main__': main()
