def add_numbers(num1, num2): return num1 + num2


def catch_error():
    try:
        add_numbers(1, '2')
    except IOError:  # this block expecting & catching only 'IOError'
        show_error(IOError.__name__)
    except TypeError as error_text:  # we can use casting if data from current error needed
        show_error(TypeError.__name__, str(error_text))
    except Exception:  # catching any errors
        print('Some other error caught!')
    else:  # 'else' block runs only when there are no error occurred in 'try' block
        print('No error occurred!')
    finally:  # 'finally' block will always be shown(if there are some errors or not)
        print('Finally block')

    # only 'try' & 'except' blocks should be if we want to catch some error(s), all others blocks are optional


def show_error(error_type: str, error_text: str = '') -> None:
    print(f'{error_type} was caught!')
    if error_text != '': print(error_text)
    print('...')


if __name__ == '__main__': catch_error()
