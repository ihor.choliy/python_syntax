# decorator is a function, that accept another function as a parameter,
# extend it with some extra functionality, and return upgraded function
def hello_decorator(hello_fun):  # you can name function argument whatever you want
    def extended_hello():
        print('Hello Python!')
        hello_fun()
        print('Hello Developer!')
        print('...')

    return extended_hello


# there is no needs to call decorator function explicitly,
# just add '@decorator_name' above function that you have to extend
@hello_decorator
def show_hello(): print('Hello World!')


# example of decorator with arguments
def info_decorator(info_fun):
    def extended_info(**kwargs):
        print('user info:')
        info_fun(kwargs['name'], kwargs['age'])
        print(f"Weight: {kwargs['weight']} kg")
        print(f"Height: {kwargs['height']} cm")

    return extended_info


@info_decorator
def show_info(name: str, age: int):
    print(f'Name: {name}\nAge: {age}')


def main():
    show_hello()
    show_info(name='Ihor', age=34, weight=84, height=182)


if __name__ == '__main__': main()
