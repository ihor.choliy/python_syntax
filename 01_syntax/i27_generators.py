# GENERATORS:
# if we are working with huge amount of data in list/sets/tuple/dict(for example with more than 100 000 items),
# all these items will be saved to the memory(RAM), and sometimes we don't want to, or we don't need them to be stored,
# but only work with one item at a time. In this case we can use generators
#
# other words if the output has potential of taking up a large amount of memory, and you only intend to iterate through it,
# without needs to hold entire data at once, you would want to use a generators

# returns the list of squared numbers with for loop(without generator)
def square_nums_v1(amount: int):
    numbers = []
    for num in range(amount): numbers.append(num ** 2)
    return numbers


# returns the list of squared numbers with list comprehension(without generator)
def square_nums_v2(amount: int):
    return [num ** 2 for num in range(amount)]


# returns one item at a time with for loop(with generator)
def square_nums_gen_v1(amount: int):
    for num in range(amount): yield num ** 2


# returns one item at a time with list comprehension(with generator)
def square_nums_gen_v2(amount: int):
    return (num ** 2 for num in range(amount))  # change [] brackets to () for converting list comprehension to generator


def main():
    # functions without generators
    print(square_nums_v1(5))
    print(square_nums_v2(5))

    # functions with generators
    numbers = square_nums_gen_v2(5)  # getting generator
    number1 = next(numbers)  # next() function will call every next iterable item from generator
    number2 = next(numbers)
    number3 = next(numbers)
    print(number1, number2, number3)

    # don't convert generator to list/dict like below,
    # otherwise we loose all generator benefits and memory efficiency,
    # if we need entire list/dict to working on, we don't need generators at all
    print(list(numbers))


if __name__ == '__main__': main()
