import unittest

import packages.firts_package.utils.string_utils as utils


# for creating custom unittest class we should extend from unittest subclass
class TestText(unittest.TestCase):
    text = 'lord of the rings'

    def test_cap(self):
        first = utils.cap_text(TestText.text)
        second = TestText.text.capitalize()
        self.assertEqual(first, second)  # unittest will be passed when first and second value will be equal

    def test_title(self):
        first = utils.title_text(TestText.text)
        second = TestText.text.title()
        self.assertEqual(first, second)  # unittest will be passed when first and second value will be equal


if __name__ == '__main__':
    unittest.main()  # call 'unittest.main()' function to start running all our test functions in test classes
