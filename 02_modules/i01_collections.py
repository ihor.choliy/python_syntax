from collections import Counter
from collections import defaultdict
from collections import namedtuple


def counter():
    numbers = [2, 4, 1, 1, 3, 3, 2, 4, 4, 5, 1, 1, 2, 2, 2, 5, 3]
    mixed_list = ['a', 0, 2, 5, 'a', 'a', 'b', 2, 'b', 4, 5, 5, 2, 'a']
    text_list = 'hello! hello! hello! how are you? are you ok? really? ok! fine!'.split()
    letters = 'dswqdaqewsqedaqwsedsqseawqdsewqsdwqaewqseawqdwqsedqawsewq'

    # Counter shows amount of items in the list and their count in descending order
    print(Counter(numbers))
    print(Counter(mixed_list))
    print(Counter(text_list))
    print(Counter(letters))
    print(Counter(letters).most_common(2))  # provides two most common elements and their counts
    print('...')

    counter_list = list(Counter(letters))  # convert counter to list
    counter_dict = dict(Counter(letters))  # convert counter to dictionary
    print(counter_list)
    print(counter_dict)


def default_dict():
    # defaultdict() function returns dictionary with default value when wrong key was passed
    # you could use it only for empty dictionary, which will accept values in the future
    dictionary = defaultdict(lambda: 0)
    dictionary.update({'a': 1, 'b': 2, 'c': 3, 'd': 4, 'e': 5})
    print(dictionary['wrong_key'], dictionary['c'])


def named_tuple():
    # super simple way to create an object based on tuple
    Monster = namedtuple('Monster', ['name', 'race', 'stamina', 'weapon'])
    monster = Monster('Azog', 'Orc', 2500, 'Axe')
    print(monster)


def main():
    # counter()
    # default_dict()
    named_tuple()


if __name__ == '__main__': main()
