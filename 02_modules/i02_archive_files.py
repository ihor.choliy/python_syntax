import os
import shutil

file_path = 'C:\\Users\\ihorc\\Desktop\\'
file_name_01 = 'file_01.txt'
file_name_02 = 'file_02.txt'
file_path_01 = file_path + file_name_01
file_path_02 = file_path + file_name_02
folder_name = 'folder_for_archive'
folder_path = file_path + folder_name


def create_files():
    file_01 = open(file_path_01, 'w+')  # create new file if it doesn't exist, or update if it's already created
    file_02 = open(file_path_02, 'w+')
    file_01.write('some text in file_01')
    file_02.write('some text in file_02')
    file_01.close()  # after finishing working with current file we have to close() it
    file_02.close()  # otherwise we wouldn't have possibility to remove or edit it from other app


def create_dir():
    try:
        os.mkdir(folder_path)
    except Exception:
        print('this folder is already exist'.upper())
    finally:
        print(os.listdir())  # prints all directories by current path


def move_files():
    move_file(file_path_01)
    move_file(file_path_02)


def move_file(file_name: str):
    try:
        shutil.move(file_name, folder_path)
    except Exception:
        print('this file is already exist'.upper())


def archive_dir():
    try:
        shutil.make_archive(folder_name, 'zip', folder_path)
    except Exception:
        print('no such folder'.upper())
    finally:
        shutil.rmtree(folder_path, True)  # remove archived folder


def move_dir():
    file_name = folder_name + '.zip'
    try:
        shutil.move(file_name, 'D:\\')
    except Exception:
        os.remove(file_name)  # remove duplicated archive
        print('destination path already exist'.upper())


def unpack_dir():
    # first parameter  - archive location
    # second parameter - unpacking destination
    shutil.unpack_archive(f'D:\\{folder_name}.zip', 'D:\\')
    print('archive created'.upper())
    print('archive unpacked'.upper())


def main():
    create_files()
    create_dir()
    move_files()
    archive_dir()
    move_dir()
    # unpack_dir()


if __name__ == '__main__': main()
