from datetime import datetime


def date_time():
    dt = datetime.now()
    print(dt)
    print(dt.year, dt.month, dt.day, dt.hour, dt.minute, dt.second, dt.microsecond)
    print(dt.ctime())
    print('...')

    date = datetime.now().date()
    print(date)
    print('...')

    time = datetime.now().time()
    print(time)


if __name__ == '__main__': date_time()
