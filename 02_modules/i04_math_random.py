import math
import random


# prints all math module functions and there short description
def print_math_docs(): help(math)


def rounding():
    value = 7.3519
    print(round(value))  # round mathematically(1.49 = 1, 1.51 = 2)
    print(round(value, 3))  # 3 - is amount of numbers after decimal point
    print(round(value, 2))
    print(round(value, 1))
    print(math.floor(value))  # round to current integer
    print(math.ceil(value))  # round to next integer


def constants():
    print(math.pi)
    print(math.e)
    print(math.inf)
    print(math.nan)


def functions():
    print(math.sin(10))
    print(math.degrees(5))  # radians to degrees
    print(math.radians(180))  # degrees to radians


def random_functions():
    print(random.randint(-100, 100))  # random int number in current range
    print(random.uniform(a=-100, b=100))  # random float number in current range

    my_list = list(range(0, 20))
    random_item = random.choice(my_list)  # pick random item from the list(without removing it)
    print(random_item)

    # pick 10 items from the list with possible duplications
    with_duplications = random.choices(population=my_list, k=10)
    print(with_duplications)

    # pick 10 items from the list without possible duplications
    without_duplications = random.sample(population=my_list, k=10)
    print(without_duplications)


def main():
    # print_math_docs()
    # rounding()
    # constants()
    functions()
    # random_functions()


if __name__ == '__main__': main()
