import re


#   REGEX EXPRESSIONS
# |--------------------|------------------------------|---------------------------------------------------|
# | CHARACTER          | DESCRIPTION                  | PATTERN                      | MATCH              |
# |--------------------|------------------------------|---------------------------------------------------|
# | \d                 | digit                        | file_\d\d OR file_\d{2}      | file_25            |
# |--------------------|------------------------------|---------------------------------------------------|
# | \w                 | alphanumeric                 | \w-\w\w\w OR \w-\w{3}        | A-b_1 OR F-You     |
# |--------------------|------------------------------|---------------------------------------------------|
# | \s                 | whitespace                   | \w\s\w\s\w                   | a b c OR 1 2 3     |
# |--------------------|------------------------------|---------------------------------------------------|
# | \D                 | not digit(only characters)   | \D\D\D OR \D{3}              | lol OR ABC         |
# |--------------------|------------------------------|---------------------------------------------------|
# | \W                 | symbol                       | \W\W\W\W OR \W{4}            | */-= OR !$&?       |
# |--------------------|------------------------------|---------------------------------------------------|
# | \S                 | non-whitespace               | \S\S\S\S\S OR \S{5}          | Crazy OR proxy     |
# |--------------------|------------------------------|---------------------------------------------------|

#   REGEX QUANTIFIERS
# |--------------------|------------------------------|---------------------------------------------------|
# | CHARACTER          | DESCRIPTION                  | PATTERN                      | MATCH              |
# |--------------------|------------------------------|---------------------------------------------------|
# | *                  | occurs zero or more          | \w*\w*\w*                    | aaaBbbCCCcc        |
# |--------------------|------------------------------|---------------------------------------------------|
# | +                  | occurs ones or more          | \w+\s\w-\d+                  | version a-1475     |
# |--------------------|------------------------------|---------------------------------------------------|
# | ?                  | occurs ones or none          | plurals?                     | plural             |
# |--------------------|------------------------------|---------------------------------------------------|
# | {3}                | occurs exactly 3 times       | \D{3}                        | you OR Hey         |
# |--------------------|------------------------------|---------------------------------------------------|
# | {2,5}              | occurs 2 to 5 times          | \d{2,5}                      | 37 OR 37513 OR 132 |
# |--------------------|------------------------------|---------------------------------------------------|
# | {4,}               | occurs 4 or more             | \w{4,}                       | Ihor OR substance  |
# |--------------------|------------------------------|---------------------------------------------------|

def number_regex():
    phone_numbers = 'This is my phone numbers: (098) 256-9624 and (067) 147-2237'
    number_pattern = r'\W\d{3}\W\s\d{3}-\d{4}'  # we have to add 'r' before string for converting it to regex
    number = re.search(number_pattern, phone_numbers)  # first matching result
    print(number)  # matching object
    print(number.group())  # matching string
    print(number.span())  # matching tuple with start/end indices
    print('...')

    numbers = re.findall(number_pattern, phone_numbers)  # list of all matching results
    print(numbers)


def extra_syntax():
    print(re.findall(r'cat|dog', 'The cat is here'))  # '|' means or
    print(re.findall(r'.at', 'The cat in the hat sat splat'))  # one character before 'at'
    print(re.findall(r'...at', 'The cat in the hat sat splat'))  # three characters before 'at'
    print(re.findall(r'^\d', '2 - this is number two'))  # '^' means start with
    print(re.findall(r'\d$', 'this is number 3'))  # '$' means ends with
    print(re.findall(r'[^\d\s]', 'this is 5, and this is 7.'))  # everything that is not a number & whitespace
    print(re.findall(r'[^\w]+', 'too, many! symbols here?'))  # list of words without symbols and whitespaces('[]' means a group)
    print(re.findall(r'[\w]+-[\w]+', 'please find hyphen-words like one-two or 3-7'))  # list of hyphen-words


def email_regex():
    emails = 'My emails: ihor.choliy@gmail.com, choliy.igor@gmail.io, elfigorr@gmail.zyz'
    email_pattern = r'[\w\.-]+@[\w\.-]+'
    emails = re.findall(email_pattern, emails)
    print(emails)


def main():
    # number_regex()
    # extra_syntax()
    email_regex()


if __name__ == "__main__": main()
