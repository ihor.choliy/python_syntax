import time


def function_01(range_size: int):
    return [num * 999 / 777 for num in range(range_size)]


def function_02(range_size: int):
    return list(map(lambda num: num * 999 / 777, range(range_size)))


def performance_check(function):
    start_time = time.time()
    function(10_000_000)
    end_time = time.time()
    print(f'Time of {function.__name__}: {end_time - start_time}')


def main():
    performance_check(function_01)
    performance_check(function_02)


if __name__ == "__main__": main()
