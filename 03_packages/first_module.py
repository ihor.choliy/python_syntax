# module is just a python script that is located in the same directory(folder),
# function(s) of which you could use from different python scripts, that are located in the same folder
# how to import and use module function(s) is shown in 'modules_and_packages.py'
def first_function(name: str) -> None:  # specified returning type is just for example
    print(f'Hello {name}, this is first module function')


def second_function():
    print('This is second module function')
