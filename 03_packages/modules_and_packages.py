# importing functions from module(with alias):
import first_module as fm

# importing functions from module(without alias):
from first_module import first_function
from first_module import second_function

# importing modules from packages(with alias):
import utils.info_utils.info_utils as info  # you can have the same module and package name
import utils.math_utils.calc_utils as calc
import utils.string_utils.text_utils as text

# importing modules from packages(without alias):
from utils.info_utils import info_utils
from utils.math_utils import calc_utils
from utils.string_utils import text_utils


# syntax below is how you can import all functions from current module/package,
# although it's bad practice, and you have to do it only if you need all functionality from current module/package
# from first_module import *

def main():
    # calling module functions by alias(second line)
    fm.first_function('Ihor')
    fm.second_function()
    print('...')

    # calling module functions without alias(5-6 lines)
    first_function('Ihor')
    second_function()
    print('...')

    # calling functions by alias from package(9-11 lines)
    info.person_info()
    info.work_info()
    calc.add_nums(3, 7)
    calc.subtract_nums(3, 7)
    text.show_length('Hello Python')
    text.show_list('Hello Python')
    print('...')

    # calling functions from packages without alias(14-16 lines)
    info_utils.person_info()
    calc_utils.add_nums(1, 2)
    text_utils.show_inverted('How are you')


if __name__ == '__main__': main()
