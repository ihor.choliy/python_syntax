def show_length(text: str): print(len(text))


def show_list(text): print(list(text))


def show_inverted(text): print(text[::-1])


def cap_text(text: str): return text.capitalize()


def title_text(text: str): return text.title()
