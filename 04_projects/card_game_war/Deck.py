import random

from Card import Card
from Data import ranks
from Data import suits


class Deck:
    cards: list[Card]

    def __init__(self):
        self.fill_cards_method_01()
        self.shuffle()

    # fill cards with list comprehensions
    def fill_cards_method_01(self):
        self.cards = [Card(suit, rank) for suit in suits for rank in ranks]

    # fill cards with for loops
    def fill_cards_method_02(self):
        for suit in suits:
            for rank in ranks:
                self.cards.append(Card(suit, rank))

    def shuffle(self):
        random.shuffle(self.cards)
