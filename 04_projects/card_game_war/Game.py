from Card import Card
from Player import Player


class Game:
    round_num = 0
    player_one_stack: list[Card] = []
    player_two_stack: list[Card] = []

    def __init__(self, cards: list[Card]):
        self.cards = cards
        self.player_one, self.player_two = self.get_players()

    def start_game(self):
        self.show_start_info()
        while True:
            # stop game if some player have no more cards
            if self.check_cards() != '':
                print(self.check_cards())
                break

            self.start_new_round()
            self.check_round_winner()

        # try again option
        self.try_again()

    def get_players(self) -> (Player, Player):
        name_one, name_two = self.get_names()
        card_one, card_two = self.get_cards()
        return Player(name_one, card_one), Player(name_two, card_two)

    def get_names(self) -> (str, str):
        name_one = input('Please enter name for player One: ')
        name_two = input('Please enter name for player Two: ')
        return name_one.upper(), name_two.upper()

    def get_cards(self) -> (list[Card], list[Card]):
        half = len(self.cards) // 2
        return self.cards[:half], self.cards[half:]

    def show_start_info(self):
        self.divider()
        print(f'The name on player One is: {self.player_one.name}')
        print(f'The name on player Two is: {self.player_two.name}')
        self.divider()
        print('All players have the same amount of cards, lets start!')

    def check_cards(self) -> str:
        if self.player_one.no_more_cards():
            return f'...\n{self.player_one}. Player {self.player_two.name} Wins!'
        elif self.player_two.no_more_cards():
            return f'...\n{self.player_two}. Player {self.player_one.name} Wins!'
        else:
            return ''

    def start_new_round(self):
        # show round number
        self.round_num += 1
        print(f'...\nRound number {self.round_num}')
        self.divider()

        # show amount of player cards
        print(f'Player {self.player_one.name} has {len(self.player_one.cards)} cards')
        print(f'Player {self.player_two.name} has {len(self.player_two.cards)} cards')
        self.divider()

        # player one move
        player_one_card = self.player_one.get_card()
        self.player_one_stack.append(player_one_card)
        print(f'Player {self.player_one.name} put on the table: {player_one_card}')

        # player two move
        player_two_card = self.player_two.get_card()
        self.player_two_stack.append(player_two_card)
        print(f'Player {self.player_two.name} put on the table: {player_two_card}')

    def check_round_winner(self):
        self.divider()
        have_winner = False
        # player one wins
        if self.player_one_stack[-1].value > self.player_two_stack[-1].value:
            print(f'Player {self.player_one.name} wins round number {self.round_num}')
            self.player_one.add_cards(self.player_one_stack + self.player_two_stack)
            have_winner = True
        # player two wins
        elif self.player_one_stack[-1].value < self.player_two_stack[-1].value:
            print(f'Player {self.player_two.name} wins round number {self.round_num}')
            self.player_two.add_cards(self.player_two_stack + self.player_one_stack)
            have_winner = True
        # no winner in this round
        else:
            print("There were no winner in this round, let's move one...")

        # clear cards stacks if there were no winner
        if have_winner:
            self.player_one_stack = []
            self.player_two_stack = []

    def try_again(self):
        again = input('Wanna try one more time? y/n: ')
        if again.lower() == 'y':
            self.player_one, self.player_two = self.get_players()
            self.start_game()
        else:
            print('Good bye!')

    def divider(self):
        print('--------------------')
