from Card import Card


class Player:

    def __init__(self, name: str, cards: list[Card]):
        self.name = name
        self.cards = cards

    def __str__(self):
        return f'Player {self.name} has {len(self.cards)} cards'

    def get_card(self):
        return self.cards.pop(0)

    def add_cards(self, cards: list[Card]):
        self.cards.extend(cards)

    def no_more_cards(self):
        return len(self.cards) == 0
