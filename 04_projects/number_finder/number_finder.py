import os
import re
import shutil

file_name = 'extract_me'


def read_instructions():
    shutil.unpack_archive(file_name + '.zip')
    print('\nInstructions:\n')
    with open(file_name + '\\instructions.txt') as file: print(file.read())


def show_results():
    print('\nSearch result:')
    print(scan_files())


def scan_files() -> []:
    results = []
    for folder, sub_folder, files in os.walk(os.getcwd() + '\\' + file_name):
        for file in files:
            file_path = folder + '\\' + file
            if search_number(file_path) is not None:
                results.append(search_number(file_path))
    return results


def search_number(text_file):
    file = open(text_file, 'r')
    text = file.read()
    pattern = r'\d{3}-\d{3}-\d{4}'
    if re.search(pattern, text):
        return re.search(pattern, text).group()
    else:
        pass


def remove_folder():
    shutil.rmtree(file_name, True)


def main():
    read_instructions()
    show_results()
    remove_folder()


if __name__ == '__main__': main()
