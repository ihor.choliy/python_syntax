from random import shuffle


def get_shuffled_ball():
    ball_position = ['O', ' ', ' ']
    shuffle(ball_position)
    return ball_position


def player_guess():
    print('...')
    print(['1', '2', '3'])
    input_number = input('Please guess ball position, enter 1, 2 or 3: ')
    while input_number not in ['1', '2', '3']:
        input_number = input('Please enter only 1, 2 or 3 numbers: ')

    return int(input_number)


def check_guess(shuffled_ball: [], input_number: int):
    if shuffled_ball[input_number - 1] == 'O':
        print(shuffled_ball)
        print('Correct!')
    else:
        print(shuffled_ball)
        print('Wrong!')


def try_again():
    again = input('Wanna try one more time? y/n: ')
    if again.lower() == 'y':
        start_game()
    else:
        print('Good bye!')


def start_game():
    shuffled_ball = get_shuffled_ball()
    input_number = player_guess()
    check_guess(shuffled_ball, input_number)
    try_again()


if __name__ == '__main__': start_game()
