import os

symbols: []
player_symbol: str


def reset_table():
    global symbols, player_symbol
    symbols = list('#' + ' ' * 9)
    player_symbol = 'O'


def show_table():
    os.system('cls')  # clear console before show updated table
    print('-------------------')
    print(f'|  {symbols[7]}  |  {symbols[8]}  |  {symbols[9]}  |')
    print('-------------------')
    print(f'|  {symbols[4]}  |  {symbols[5]}  |  {symbols[6]}  |')
    print('-------------------')
    print(f'|  {symbols[1]}  |  {symbols[2]}  |  {symbols[3]}  |')
    print('-------------------')


def show_player_name():
    print(f"Step of the {get_player_name()}")


def get_player_name():
    return 'Player 1' if player_symbol == 'O' else 'Player 2'


def show_start_info():
    return input('Please enter number from 1 to 9, positions the same as in numpad: ')


def validate_number(number: str):
    while not number.isdigit() or not int(number) in range(1, 10):
        number = input('Please enter valid number, from 1 to 9: ')
    return number


def validate_socket(number: str):
    while symbols[int(number)] != ' ':
        number = input('There is already character in current position, please pick another one: ')
        number = validate_number(number)
    return number


def set_symbol(number: str):
    symbols[int(number)] = player_symbol


def check_results():
    if end_game():
        show_winner()
        try_again()
    elif no_more_sockets():
        print('No more empty sockets')
        try_again()
    else:
        switch_player()
        show_player_name()
        start_game()


def end_game():
    pattern = player_symbol * 3
    if (
            f'{symbols[1]}{symbols[2]}{symbols[3]}' == pattern) or (
            f'{symbols[4]}{symbols[5]}{symbols[6]}' == pattern) or (
            f'{symbols[7]}{symbols[8]}{symbols[9]}' == pattern) or (
            f'{symbols[1]}{symbols[4]}{symbols[7]}' == pattern) or (
            f'{symbols[2]}{symbols[5]}{symbols[8]}' == pattern) or (
            f'{symbols[3]}{symbols[6]}{symbols[9]}' == pattern) or (
            f'{symbols[1]}{symbols[5]}{symbols[9]}' == pattern) or (
            f'{symbols[7]}{symbols[5]}{symbols[3]}' == pattern):
        return True
    else:
        return False


def no_more_sockets():
    empty_sockets = 0
    for item in symbols:
        if item == ' ': empty_sockets += 1
    return True if empty_sockets == 0 else False


def show_winner():
    print('Great pick!')
    print(f'The winner is {get_player_name()}')


def try_again():
    again = input('Wanna try one more time? y/n: ')
    if again.lower() == "y":
        play()
    else:
        print('Good bye!')


def switch_player():
    global player_symbol
    if player_symbol == 'O':
        player_symbol = 'X'
    else:
        player_symbol = 'O'


def start_game():
    number = show_start_info()
    number = validate_number(number)
    number = validate_socket(number)
    set_symbol(number)
    show_table()
    check_results()


def play():
    reset_table()
    show_table()
    show_player_name()
    start_game()


if __name__ == '__main__': play()
